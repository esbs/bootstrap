# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.3.0] - 2022-05-25
### Fixed
- tftpboot: don't start docker image if tftpboot directory does not exist
- bootstrap: warn if image is already running


### Changed
- Replace ISC DHCP server with dnsmasq


### Added
- ftps: Support implicit FTPS on port 990
- bootstrap: Add FTP support
- bootstrap: Enable remote logging


## [v1.2.0] - 2022-03-04
### Fixes
- imx_usb: Rely on /sys/bus/usb/devices/ instead of /sys/bus/hid/devices/
- shellcheck: compliance with v0.8.0
- bootstrap: warn if image is already running
- tftpboot: don't start docker image if tftpboot directory does not exist


### Added
- bootstrap: Enable remote logging


## [v1.1.0] - 2021-02-04
### Fixes
- Simpler multiarch releases
- Sync and Cleanup

### Added
- USB boot support for i.MX using imx_usb_loader


## [v1.0.0] - 2019-08-09
### Added
- Add support for sitara 335x series bootrom
- Add a few simple convenience functions
- Create basic bootstrapping infrastructure
