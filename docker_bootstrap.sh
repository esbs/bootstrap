#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

WORKDIR="${WORKDIR:-/bootstrap/}"
REQUIRED_COMMANDS='
	[
	command
	docker
	echo
	eval
	exit
	getopts
	hostname
	printf
	pwd
	readlink
	set
	shift
'


_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

usage()
{
	echo "Usage: ${0} -d <DHCPD_IFACE> [OPTIONS] [command [arguments...]]"
	echo 'A wrapper script to instantiate the bootstrap container.'
	echo '    -b  Bootstrapping path [BOOTSTRAP_PATH]'
	echo '    -d  Run docker detached'
	echo '    -f  FTP server path [FTP_PATH]'
	echo '    -h  Print usage'
	echo '    -i  Required network interface to bind to [DHCPD_IFACE]'
	echo '    -k  Key prefix (</path/to/file>.key and certificate </path/to/file>.crt) to use, re-generated otherwise [SSL_KEY_PREFIX]'
	echo '    -N  Enable NAT and forward networking [NAT_IFACE]'
	echo '    -n  Mount volume to use as an NFS share (Ex. /tmp) [NFS_PATH]'
	echo '    -m  Name for the container [CONTAINER_NAME]'
	echo '    -r  Supply a custom registry image [CI_REGISTRY_IMAGE]'
	echo '    -s  Docker argument for --restart (no, on-failure[:max-retries], always or unless-stopped) [DOCKER_RESTART]'
	echo '    -w  WWW server path [WWW_PATH]'
	echo
	echo 'All options can also be passed in environment variables (listed between [brackets]).'
}

add_volume()
{
	_src_path="${1:?Missing argument to function}"
	_dest_path="${2:-${_src_path}}"

	if [ "${_src_path%'~'*}" != "${_src_path}" ]; then
		eval _src_path="${_src_path}"
	fi
	if [ -L "${_src_path}" ]; then
		_src_path="$(readlink -f "${_src_path}")"
	fi
	if [ "${_src_path#'/'*}" = "${_src_path}" ]; then
		__src_abs_path="$(pwd)/${_src_path}"
		if [ -d "${__src_abs_path}" ]; then
			_src_path="${__src_abs_path}"
		else
			echo "Using path '${_src_path}' as container volume."
		fi
	fi
	opt_docker_args="--volume '${_src_path}:${_dest_path}' ${opt_docker_args}"
}

init()
{
	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%"${src_file##*'/'}"}"

	# shellcheck disable=SC2021  # Busybox tr is non-posix without classes
	ci_registry_image="${ci_registry_image:-${CI_REGISTRY_IMAGE:-$(basename "${src_dir}" | \
	                                                               tr '[A-Z]' '[a-z]'):latest}}"

	if [ -n "$(docker container list \
	                  --all \
	                  --filter ancestor="${ci_registry_image}" \
	                  --filter status='running' \
	                  --quiet || \
	           true)" ]; then
		e_warn "Container '${ci_registry_image}' already running on '$(hostname || true)'."
	fi

	opt_docker_args="${OPT_DOCKER_ARGS:-}"
	opt_docker_args="--env 'NAT_IFACE=${nat_iface}' ${opt_docker_args}"
	opt_docker_args="--env 'DHCPD_IFACE=${DHCPD_IFACE:?Place provide a network interface.}' ${opt_docker_args}"

	if [ -n "${bootstrap_path:-}" ]; then
		add_volume "${bootstrap_path}" "${WORKDIR}"
	fi

	if [ -n "${ftp_path:-}" ]; then
		add_volume "${ftp_path}" "${WORKDIR}/ftp"
	fi

	if [ -n "${nfs_path:-}" ]; then
		add_volume "${nfs_path}" "${WORKDIR}/nfs"
	fi

	if [ -n "${ssl_key_prefix:-}" ]; then
		add_volume "$(dirname "${ssl_key_prefix}")"
		opt_docker_args="--env 'SSL_KEY_PREFIX=${ssl_key_prefix}' ${opt_docker_args}"
	fi

	if [ -n "${www_path:-}" ]; then
		add_volume "${www_path}" "${WORKDIR}/www"
	fi

	if ! docker pull "${ci_registry_image}" 2> '/dev/null'; then
		e_warn "Unable to pull docker image '${ci_registry_image}', building locally instead."
		if ! docker build \
		            --file "${src_dir}/Containerfile" \
		            --pull \
		            --tag "${ci_registry_image}" \
		            "${src_dir}"; then
			e_warn 'Failed to build local container, attempting existing container.'
		fi
	fi

	if ! docker image inspect "${ci_registry_image}" 1> '/dev/null'; then
		e_err "Container '${ci_registry_image}' not found, cannot continue."
		exit 1
	fi
}

run_script()
{
	# `--rm` and restart are mutually exclusive options
	if [ -z "${docker_restart:-}" ]; then
		__restart='--rm'
	else
		__restart="--restart '${docker_restart}'"
	fi
	eval docker run \
	            --hostname "$(hostname || true)" \
	            --interactive \
	            --network 'host' \
	            --privileged \
	            --tty \
	            --volume '/dev/bus/usb/:/dev/bus/usb/' \
	            --workdir "${WORKDIR}" \
	            ${__restart:+${__restart}} \
	            ${container_name:+--name "${container_name}"} \
	            ${docker_detach:+--detach} \
	            "${opt_docker_args}" \
	            "${ci_registry_image}" \
	            "${*}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
	    e_err 'Self-test failed, missing dependencies.'
	    echo '======================================='
	    echo 'Passed tests:'
	    # As the results contain \n, we expect these to be interpreted.
	    # shellcheck disable=SC2059
	    printf "${_test_result_pass:-none\n}"
	    echo '---------------------------------------'
	    echo 'Failed tests:'
	    # shellcheck disable=SC2059
	    printf "${_test_result_fail:-none\n}"
	    echo '======================================='
	    exit 1
	fi
}

main()
{
	while getopts ':b:df:hi:k:Nn:m:r:s:w:' _options; do
		case "${_options}" in
		b)
			bootstrap_path="${OPTARG}"
			;;
		d)
			docker_detach='true'
			;;
		f)
			ftp_path="${OPTARG}"
			;;
		h)
			usage
			exit 0
			;;
		i)
			DHCPD_IFACE="${OPTARG}"
			;;
		k)
			ssl_key_prefix="${OPTARG}"
			;;
		N)
			nat_iface="${OPTARG}"
			;;
		n)
			nfs_path="${OPTARG}"
			;;
		m)
			container_name="${OPTARG}"
			;;
		r)
			ci_registry_image="${OPTARG}"
			;;
		s)
			docker_restart="${OPTARG}"
			;;
		w)
			www_path="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	bootstrap_path="${bootstrap_path:-${BOOTSTRAP_PATH:-$(pwd)}}"
	container_name="${container_name:-${CONTAINER_NAME:-}}"
	docker_restart="${docker_restart:-${DOCKER_RESTART:-}}"
	ftp_path="${ftp_path:-${FTP_PATH:-}}"
	nat_iface="${nat_iface:-${NAT_IFACE:-}}"
	nfs_path="${nfs_path:-${NFS_PATH:-}}"
	ssl_key_prefix="${ssl_key_prefix:-${SSL_KEY_PREFIX:-}}"
	www_path="${www_path:-${WWW_PATH:-}}"

	check_requirements
	init
	run_script "${@}"
}

main "${@}"

exit 0
