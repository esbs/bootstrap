#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# This script is inspired by https://get.docker.com and intended
# to do a quick & dirty install.

set -eu

BOOTSTRAP_ARCHIVE="${BOOTSTRAP_ARCHIVE:-}"
VERSION='@GIT_VERSION@'


interface_setup()
{
	_found=0
	for _iface in '/sys/class/net/'*; do
		_iface="${_iface##*'/'}"
		if [ "${_iface}" = 'lo' ]; then
			continue
		fi

		_ip_address="$(ip -o -4 addr show dev "${_iface}" | \
		               cut -d ' ' -f 7 | \
		               cut -f 1 -d '/')"
		echo "Interface '${_iface}' (${_ip_address:-available})"
		_found="$((_found + 1))"
	done

	if [ "${_found}" -eq 0 ]; then
		echo 'No available interfaces found, at least one, but preferably two interfaces are required.'
		exit 1
	fi

	if [ "${_found}" -eq 1 ]; then
		echo 'Only one available interface was found. By continuing this interface'
		echo 'will be reconfigured with a static IP address. Ensure it is not connected to the LAN!'
		echo 'Also internet connectivity will be lost! And the main script will'
		echo 'fail without at least being able to connect once! Use this at'
		echo 'own risk!'
	fi

	echo 'Do you wish to reconfigure one of the above interfaces for bootstrap use? (y/N)'
	read -r _iface_reconf
	case "${_iface_reconf}" in
	[yY]*)
		return
		;;
	*)
		echo 'Exiting.'
		exit 0
		;;
	esac

	echo "Enter interface to reconfigure (Use 'exit' to exit)."
	while true; do
		read -r iface_req
		if [ "${iface_req}" = 'exit' ]; then
			exit 0
		fi

		if [ ! -d "/sys/class/net/${iface_req}" ]; then
			echo "Entered interface is not valid '${iface_req}'."
			continue
		fi

		break
	done

	echo "iface ${iface_req} inet manual" > '/etc/network/interfaces.d/bootstrap_claimed'

	echo "Configured '${iface_req}' in '/etc/network/interfaces.d/bootsrap_claimed."
	echo 'To undo this configuration, simply remove the above file.'
}

main()
{
	if [ "$(id -u || true)" -ne 0 ]; then
		echo 'This script requires elevated privileges.'
		echo "Either run as root, or prefixed with 'sudo'."
		exit 1
	fi

	echo 'Setting up interface'
	interface_setup

	echo "Getting latest bootstrap script version ${VERSION}."
	wget "${BOOTSTRAP_ARCHIVE:?}" -O '/tmp/basic_bootstrap.tar.bz2'
	mkdir -p '/opt/bootstrap/'
	tar -xvf '/tmp/basic_bootstrap.tar.bz2' --strip-components=1 -C '/opt/bootstrap/'

	if [ ! -x '/opt/bootstrap/docker_bootstrap.sh' ]; then
		echo "The script 'docker_bootstrap.sh' was not found executable."
		echo "This shouldn't have happened. Error, cannot continue."
		exit 1
	fi

	echo 'Installing docker, this can take up to 5 minutes.'
	wget 'https://get.docker.com' -O - | sh
	usermod -aG docker "$(logname || true)"

	echo "All done, files installed into '/opt/bootstrap/'."
	echo 'To use, change the path to the directory serving files and run:'
	echo "  /opt/bootstrap/docker_bootstrap.sh -i ${iface_req:?}"
	echo 'To ensure everything comes up as intended, a reboot is recommended.'
}

main "${@}"

exit 0
